#include <chrono>
#include <iostream>
#include <vector>

using namespace std;
using namespace std::chrono;

struct S {
    uint64_t u;
    double d;
    int i;
    float f;
};

struct Data {
    vector<uint64_t> vu;
    vector<double> vd;
    vector<int> vi;
    vector<float> vf;
};

int test1(S const &s1, S const &s2) {
    return s1.i + s2.i;
}

int test2(Data const &data, size_t const ind1, size_t const ind2) {
    return data.vi[ind1] + data.vi[ind2];
}

int main() {
    size_t const N{50000};
    size_t const R{5};

    vector<S> v(N);
    Data data;
    data.vu.resize(N);
    data.vd.resize(N);
    data.vi.resize(N);
    data.vf.resize(N);

    int result{0};
    long double sum1 = 0;
    long double sum2 = 0;

    cout << "test #1" << endl;
    for (uint32_t i{0}; i < R; ++i) {
        auto const start{high_resolution_clock::now()};
        for (size_t a{0}; a < v.size() - 1; ++a) {
            for (size_t b{a + 1}; b < v.size(); ++b) {
                result += test1(v[a], v[b]);
            }
        }
        cout << i << ": " << duration<float>{high_resolution_clock::now() - start}.count() << endl;
        sum1 += duration<float>{high_resolution_clock::now() - start}.count();
    }

    cout << "test #2" << endl;
    for (uint32_t i{0}; i < R; ++i) {
        auto const start{high_resolution_clock::now()};
        for (size_t a{0}; a < v.size() - 1; ++a) {
            for (size_t b{a + 1}; b < v.size(); ++b) {
                result += test2(data, a, b);
            }
        }
        cout << i << ": " << duration<float>{high_resolution_clock::now() - start}.count() << endl;
        sum2 += duration<float>{high_resolution_clock::now() - start}.count();
    }

    cout << "test #1 sum is: " << sum1 << "  ->  test #2 sum is: " << sum2 << endl;
    return result;
}
